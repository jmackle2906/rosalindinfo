firstString= "AAAATTTTCCCCGGGGATCG"
secondString="AAAATGTTCCACGGCGATCG"
def countTheDifferencesBetweenTwoStrings(string1,string2):
    count=0
    numberOfDifference=0
    for nucleotide in string1:
        if nucleotide != string2[count]:
            numberOfDifference+=1
        count+=1
    print(numberOfDifference)

def openFileCountDifferences(filename):
    with open(filename) as infile:
        infile=infile.readlines()
        countTheDifferencesBetweenTwoStrings(infile[0],infile[1])