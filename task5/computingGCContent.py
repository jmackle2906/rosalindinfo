#acquireString
import re
instring = """>Rosalind_6404
CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC
TCCCACTAATAATTCTGAGG
>Rosalind_5959
CCATCGGTAGCGCATCCTTAGTCCAATTAAGTCCCTATCCAGGCGCTCCGCCGAAGGTCT
ATATCCATTTGTCAGCAGACACGC
>Rosalind_0808
CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGACCAGCCCGGAC
TGGGAACCTGCGGGCAGTAGGTGGAAT"""

def stringPreprocessing(stringProcessing):
    stringProcessing = stringProcessing.split(">")
    for items in stringProcessing:
        stringProcessing[stringProcessing.index(items)]=items.replace("\n","")
    return stringProcessing

def findTheHighestGCContent(nucleotideString):
    biggestGCpercentage = 0
    StringName = ""
    preprocessedString= stringPreprocessing(nucleotideString)
    for nucleotideStrings in preprocessedString:
        processedString=re.split("([0-9]{4})", nucleotideStrings)
        if len(processedString) == 3:
            gcPercentage = (100 * (processedString[2].count("C")+processedString[2].count("G")) / len(processedString[2]))
            if gcPercentage>biggestGCpercentage:
                biggestGCpercentage = gcPercentage
                StringName=processedString[0]+processedString[1]
    print(StringName,"\n",biggestGCpercentage)

def openFileAndFindLargestGC(filename):
    with open (filename,"r") as infile:
        data= infile.read()
        data = ''.join(data)
        findTheHighestGCContent(data)

openFileAndFindLargestGC("rosalind_gc (1).txt")



