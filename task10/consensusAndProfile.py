#preprocess

import re

def preprocessData (text):
    readTxt = text.replace("\n", "")
    fileArray = re.split("(>Rosalind_[0-9]{1,})", readTxt)
    return fileArray

def generateEmptyNucleotideArrays (nucreotideString):
    a = [0] * (len(nucreotideString))
    t = [0] * (len(nucreotideString))
    g = [0] * (len(nucreotideString))
    c = [0] * (len(nucreotideString))
    return[a, c, g, t]

def countEachNucleotide(emptyNucleotideArray,arrayOfSequences):
    printArray=emptyNucleotideArray
    seqArray=arrayOfSequences
    i = 0
    a = printArray[0]
    c = printArray[1]
    g = printArray[2]
    t = printArray[3]
    while i < len(seqArray[0]):
        for nucleotides in seqArray:
            if nucleotides[i] == "A":
                a[i] = a[i] + 1
            if nucleotides[i] == "T":
                t[i] = t[i] + 1
            if nucleotides[i] == "G":
                g[i] = g[i] + 1
            if nucleotides[i] == "C":
                c[i] = c[i] + 1

        i += 1
    return[a,c,g,t]


def generateSequenceArrayAndCreateEmptyArrays():
    global seqArray, printArray
    seqArray = []
    arraysMade = False
    for nucSeq in fileArray:
        if len(re.findall("[ATCG]{3,}", nucSeq)) > 0:
            if arraysMade == False:
                printArray = generateEmptyNucleotideArrays(nucSeq)
                arraysMade == True
            seqArray.append(nucSeq)


def generatingAllArrays():
    global printArray, nucleotideID, nus
    printArray = countEachNucleotide(printArray, seqArray)
    nucleotideID = ["A:", "C:", "G:", "T:"]
    nus = ["A", "C", "G", "T"]


def printMajoritySequence():
    count = 0
    while count < len(printArray[0]):
        maxnumber = [printArray[0][count], printArray[1][count], printArray[2][count], printArray[3][count]]
        print(nus[maxnumber.index(max(maxnumber))], end="")
        count += 1
    print("")


def printTableOfFrequency():
    IDcount = 0
    for array in printArray:
        print(nucleotideID[IDcount], end=" ")
        IDcount += 1
        for digits in array:
            print(digits, end=" ")
        print("")


with open ("eg.txt") as txtIn:
    readTxt = txtIn.read()
    fileArray =preprocessData(readTxt)
    generateSequenceArrayAndCreateEmptyArrays()
    generatingAllArrays()
    printMajoritySequence()
    printTableOfFrequency()